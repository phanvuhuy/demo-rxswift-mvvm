//
//  ViewModelType.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation

protocol ViewModelType {
  
  associatedtype Input
  associatedtype Output
  
  func transform(input: Input) -> Output
}
