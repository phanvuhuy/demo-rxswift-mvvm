//
//  Reservations.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import UIKit
import ObjectMapper

enum ReservationStatus: String {
  case finished
  case canceled
  case started
  case new
}

struct Reservation: Mappable {
  
  var id, doctorID, shiftID, channelId: Int?
  var question1, question2, question3: Int?
  var questionText1, questionText2, questionText3: String?
  var type, examinationContent, insurance: Int?
  var doctor: Doctor?
  var channelUUID: String?
  var startAt, endAt: Date?
  var status: String?
  
  init() {
    id = 0
    doctorID = 0
    shiftID = 0
    question1 = 0
    question2 = 0
    question3 = 0
    type = 0
    examinationContent = 0
    insurance = 0
    startAt = Date()
    endAt = Date()
    doctor = Doctor(name: "Doctor")
  }
  
  init?(map: Map) {
    
  }
  
  init(channelUUID: String?, doctorName: String?) {
    self.channelUUID = channelUUID
    self.doctor = Doctor(name: doctorName)
  }
  
  mutating func mapping(map: Map) {
    id  <- map["id"]
    startAt <- (map["start_at"], UTCDateTransform())
    endAt <- (map["end_at"], UTCDateTransform())
    question1 <- map["question1"]
    question2 <- map["question2"]
    question3 <- map["question3"]
    questionText1 <- map["question_text_1"]
    questionText2 <- map["question_text_2"]
    questionText3 <- map["question_text_3"]
    type  <- map["type"]
    status <- map["status"]
    examinationContent  <- map["examination_content"]
    doctor  <- map["doctor"]
    channelUUID  <- map["channel_uuid"]
    channelId  <- map["channel_id"]
  }
  
  func params() -> [String: Any]{
    var params = [String: Any]()
    params["doctor_id"] = doctorID
    params["shift_id"] = shiftID
    params["question1"] = question1
    params["question2"] = question2
    params["question3"] = question3
    params["type"] = type
    params["status"] = status
    params["examination_content"] = examinationContent
    params["insurance"] = insurance
    params["start_at"] = startAt?.dateToString(DateFormat.dateTimeWithTimeZone.rawValue)
    params["end_at"] = endAt?.dateToString(DateFormat.dateTimeWithTimeZone.rawValue)
    return params
  }
}

open class UTCDateTransform: DateFormatterTransform {
  
  public init() {
    let formatter = DateFormatter()
    formatter.locale = Locale.current
    formatter.dateFormat = DateFormat.dateTimeWithTimeZone.rawValue // "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
    super.init(dateFormatter: formatter)
  }
}
