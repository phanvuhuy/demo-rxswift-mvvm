//
//  Doctor.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation
import ObjectMapper

struct Doctor: Mappable {
  
  var id: Int?
  var name: String?
  var email: String?
  var avatarURL: String?

  init(name: String?=nil) {
    self.name = name
  }
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    id   <- map["id"]
    name <- (map["name"])
    email <- (map["email"])
    avatarURL <- (map["avatar_url"])
  }
}
