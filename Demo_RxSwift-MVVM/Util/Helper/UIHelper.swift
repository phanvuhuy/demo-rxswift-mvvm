//
//  UIHelper.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation
import SVProgressHUD

struct UIHelper {
  
  static func showLoading(_ message: String = "Please wait.") {
    DispatchQueue.main.async {
      SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
      SVProgressHUD.show(withStatus: message)
    }
  }
  
  static func hideLoading() {
    DispatchQueue.main.async {
      SVProgressHUD.dismiss()
    }
  }
}
