//
//  APIService.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

/// Network response customization
///
/// - next: if all conditions are met and response object is already
/// - error: if there has an error
/// - completed: if there has no error as well as no value
public enum NetworkResponse<T> {
  case next(T)
  case error(Error?)
  case completed
  
  public var isNext: Bool {
    if case .next(_) = self {
      return true
    }
    return false
  }
}

class APIService: NSObject {
  
  typealias ParamDict = [String: Any]
  private static let accessToken = ""
  private static let unexpectedError = "An unexpected error occurs. Please try again!"
  
  // MARK: - Request
  class public func requestAPI(_ url: String, method: HTTPMethod, parameters: ParamDict? = nil) -> Observable<NetworkResponse<ParamDict>> {
    /// header
    var headers: HTTPHeaders = ["Content-Type": "application/json"]
    headers["Authorization"] = "Bearer \(accessToken)"
    
    /// url
    let url = "https://api.dev.masai.bla-one.net/api/\(url)"
    
    return Observable.create({ (observer) -> Disposable in
      let request = Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseData { (response) in
        switch response.result {
        case .success:
          guard let data = response.data else {
            observer.onNext(.error(NSError(domain: unexpectedError, code: 400, userInfo: [:])))
            return
          }
          
          if let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) {
            if let js = json as? ParamDict, let statusCode = response.response?.statusCode {
              switch statusCode {
                /// success
              case 200...226:
                observer.onNext(.next(js))
                
                /// client error
              case 400...451:
                let message = js["message"] as? String ?? ""
                observer.onNext(.error(NSError(domain: message, code: statusCode, userInfo: [:])))
              default:
                observer.onNext(.error(NSError(domain: unexpectedError, code: statusCode, userInfo: [:])))
              }
            } else {
              observer.onNext(.error(NSError(domain: unexpectedError, code: 400, userInfo: [:])))
            }
          } else {
            let message = "Can not parse json response: \(String(data: data, encoding: .utf8) ?? "")"
            observer.onNext(.error(NSError(domain: message, code: 400, userInfo: [:])))
          }
          
        case .failure(let error as NSError):
          observer.onNext(.error(error))
        }
      }
      
      return Disposables.create {
        request.cancel()
      }
    })
  }
}
