//
//  UIImageViewExtension.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
  
  func setImageUrl(_ url: String?, placeHolder: UIImage = #imageLiteral(resourceName: "ic_default_avatar"), showIndicator: Bool = true, completion: (() -> Void)?=nil) {
    
    if showIndicator {
      self.kf.indicatorType = .activity
    }
    
    /// get key cache image
    let cacheKey = url.unWrap.components(separatedBy: "?X-Amz-Content-Sha256").first.unWrap
    guard let url = URL(string: url.unWrap) else { return }
    let source = ImageResource(downloadURL: url, cacheKey: cacheKey)
    
    self.kf.setImage(with: source,
                     placeholder: placeHolder,
                     options: [
                      .scaleFactor(UIScreen.main.scale),
                      .cacheOriginalImage]) { _ in
                        completion?()
    }
  }
}
