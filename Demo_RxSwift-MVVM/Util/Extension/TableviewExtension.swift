//
//  TableviewExtension.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//
import Foundation
import RxSwift
import RxCocoa

extension UITableView {
  
  func register<T: UITableViewCell>(_ aClass: T.Type) {
    let name = String(describing: aClass)
    let bundle = Bundle.main
    if bundle.path(forResource: name, ofType: "nib") != nil {
      let nib = UINib(nibName: name, bundle: bundle)
      register(nib, forCellReuseIdentifier: name)
    } else {
      register(aClass, forCellReuseIdentifier: name)
    }
  }
  
  func dequeue<T: UITableViewCell>(_ aClass: T.Type) -> T {
    let name = String(describing: aClass)
    guard let cell = dequeueReusableCell(withIdentifier: name) as? T else {
      fatalError("`\(name)` is not registed")
    }
    return cell
  }
  
  func dequeue<T: UITableViewCell>(_ aClass: T.Type, for indexPath: IndexPath) -> T {
    let name = String(describing: aClass)
    guard let cell = dequeueReusableCell(withIdentifier: name, for: indexPath) as? T else {
      fatalError("`\(name)` is not registed")
    }
    return cell
  }
  
  func refreshDriver() -> Driver<()>{
    if let refreshControl = refreshControl {
      return refreshControl.rx.controlEvent(.valueChanged).asDriver()
    }
    return Driver.just(())
  }
}
