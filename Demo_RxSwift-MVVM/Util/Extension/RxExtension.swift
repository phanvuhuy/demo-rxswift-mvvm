//
//  RxExtension.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import RxSwift
import RxCocoa

// MARK: - RxSwift
public protocol OptionalType {
  
  associatedtype Wrapped
  
  var optional: Wrapped? { get }
}

extension Optional: OptionalType {
  
  public var optional: Wrapped? { return self }
  public var notNil: Bool {
    return self != nil
  }
  public var isNil: Bool {
    return self == nil
  }
  
  public var unWrap: String {
    if let number = self as? NSNumber {
      return "\(number)"
    }
    return self as? String ?? ""
  }
  
  var unWrapNumber: Int {
    return self as? Int ?? 0
  }
  
  public var unWrapArray: [String] {
    return self as? [String] ?? []
  }
  
  public var unWrapDic: [String: String] {
    return self as? [String: String] ?? [:]
  }
}

public extension Observable where Element: OptionalType {
  
  func ignoreNil() -> Observable<Element.Wrapped> {
    return flatMap { value in
      value.optional.map { Observable<Element.Wrapped>.just($0) } ?? Observable<Element.Wrapped>.empty()
    }
  }
}

extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy, Element: OptionalType {
  public func ignoreNil() -> Driver<Element.Wrapped> {
    return flatMap { value in
      value.optional.map { Driver<Element.Wrapped>.just($0) } ?? Driver<Element.Wrapped>.empty()
    }
  }
}

public extension Observable {
  
  func asDriver(_ def: Element) -> Driver<Element> {
    return asDriver(onErrorJustReturn: def)
  }
  
  func void() -> Observable<Void> {
    return map { _ in }
  }
}

public extension SharedSequenceConvertibleType {
  
  func void() -> SharedSequence<SharingStrategy, Void> {
    return map{ _ in }
  }
}
