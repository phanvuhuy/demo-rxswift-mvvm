//
//  DateExtension.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation

extension Date {
  
  func dateToString(_ format: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DateFormat.dateTimeWithTimeZone.rawValue
    
    let dateString = dateFormatter.string(from: self)
    let date = dateFormatter.date(from: dateString)
    
    dateFormatter.dateFormat = format
    
    guard let d = date else { return "" }
    
    let myDate = dateFormatter.string(from: d)
    
    return myDate
  }
}

enum DateFormat: String {
  case dateTimeWithTimeZone = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
  case dateHourMinuteSecond = "yyyy-MM-dd HH:mm:ss"
  case dateHourMinute = "yyyy-MM-dd HH:mm"
  case monthDayHourMinute = "MM/dd HH:mm"
  case date           = "yyyy-MM-dd"
  case monthYear  = "MM/yy"
  case hourMinute     = "HH:mm"
  case japanYearMonthDate = "yyyy年MM月dd日"
  case japanYearMonthDateWithWeekday = "yyyy年MM月dd日(E)"
  case japanDateWithWeekday = "M/d  E"
  case japanDateWithWeekday2 = "M/d(E)"
  case japanDateTime = "yyyy年MM月dd日(E) HH:mm"
  case japanDateTimeWithoutWeekday = "yyyy年MM月dd日 HH:mm"
  case japanMonthDateTime = "MM月dd日(E) HH:mm"
  case japanMonthDate = "MM月dd日"
  case japanMonthDateWithWeekday = "MM月dd日(E)"
  case dateExamination = "yyyy/MM/dd"
  case dateExamination2 = "yyyy/MM/dd (E)"
}
