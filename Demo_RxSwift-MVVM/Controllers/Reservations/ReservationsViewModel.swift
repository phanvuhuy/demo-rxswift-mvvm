//
//  ReservationsViewModel.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

final class ReservationsViewModel: ViewModelType {
  
  private let reservationsUrl = "users/reservations"
  private let disposeBag = DisposeBag()
  private var reservations = BehaviorRelay<[Reservation]>(value: [])
  private var error = PublishSubject<String>()

  // MARK: - Transform
  func transform(input: ReservationsViewModel.Input) -> ReservationsViewModel.Output {
    /// initialize indicator
    let activity = ActivityIndicator()
    
    /// actions
    getReservations(trigger: input.refresh,activity: activity)
    deleteReservation(trigger: input.deleteReservation)
    addReservation(trigger: input.addReservation)
    
    let selectedReservation = input.selectedReservation.withLatestFrom(self.reservations.asDriver()){(indexPath, reservations) in
      return reservations[indexPath.row]
      }
    
    /// output
    return Output(reservations: reservations.asDriver(),
                  selectedReservation: selectedReservation.asDriver(),
                  error: error.asDriverOnErrorJustComplete())
  }
  
  private func getReservations(activity: ActivityIndicator) {
    APIService.requestAPI(reservationsUrl, method: .get, parameters: createParams())
      .trackActivity(activity) /// add indicator
      .asDriverOnErrorJustComplete()  /// Observeble -> Driver
      .drive(onNext: { [weak self] (response) in
        /// response is a NetworkResponse
        switch response {
        case .next(let data):
          if let reservations = Mapper<Reservation>().mapArray(JSONObject: data["data"]) {
            self?.reservations.accept(reservations)
          }
        case .error(let error):
          self?.error.onNext(error?.localizedDescription ?? "")
        default: break
        }
      }).disposed(by: disposeBag)
  }
  
  private func getReservations(trigger: Driver<()>, activity: ActivityIndicator) {
    trigger.flatMapLatest { [unowned self] _ -> Driver<NetworkResponse<APIService.ParamDict>> in
      return APIService.requestAPI(self.reservationsUrl, method: .get, parameters: self.createParams())
        .trackActivity(activity)
        .asDriverOnErrorJustComplete()
      }.drive(onNext: { [weak self] (response) in
        switch response {
        case .next(let data):
          if let reservations = Mapper<Reservation>().mapArray(JSONObject: data["data"]) {
            self?.reservations.accept(reservations)
          }
        case .error(let error):
          self?.error.onNext(error?.localizedDescription ?? "")
        default: break
        }
      }).disposed(by: disposeBag)
  }
  
  func createParams() -> [String: Any] {
    var params: [String: Any] = ["page": 1]
    params["per_page"] = 20
    params["with"] = "doctor"
    params["status_in"] = ["new", "started"]
    return params
  }
  
  private func deleteReservation(trigger: Driver<IndexPath>) {
    trigger.drive(onNext: { [unowned self] (indexPath) in
      var revs = self.reservations.value
      revs.remove(at: indexPath.row)
      self.reservations.accept(revs)
    }).disposed(by: disposeBag)
  }
  
  private func addReservation(trigger: Driver<()>) {
    trigger.drive(onNext: { [unowned self]  in
      var revs = self.reservations.value
      revs.append(Reservation())
      self.reservations.accept(revs)
    }).disposed(by: disposeBag)
  }
}

// MARK: - Input, Output
extension ReservationsViewModel {
  struct Input {
    let addReservation: Driver<()>
    let deleteReservation: Driver<IndexPath>
    let selectedReservation: Driver<IndexPath>
    let refresh: Driver<()>
  }
  
  struct Output {
    let reservations: Driver<[Reservation]>
    let selectedReservation: Driver<Reservation>
    let error: Driver<String>
  }
}
