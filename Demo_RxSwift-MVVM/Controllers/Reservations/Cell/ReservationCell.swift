//
//  ReservationCell.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import UIKit
import Kingfisher

class ReservationCell: UITableViewCell {
  
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var avatarImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
  }
  
  var data: Reservation? {
    didSet {
      if let data = data {
        let date = data.startAt?.dateToString(DateFormat.dateExamination2.rawValue)
        let startTime = data.startAt?.dateToString(DateFormat.hourMinute.rawValue)
        let endTime = data.endAt?.dateToString(DateFormat.hourMinute.rawValue)
        timeLabel.text = date.unWrap + " " + startTime.unWrap + " - " + endTime.unWrap

        nameLabel.text = data.doctor?.name
        
        if let url = data.doctor?.avatarURL {
          avatarImageView.setImageUrl(url)
        }
      }
    }
  }
}
