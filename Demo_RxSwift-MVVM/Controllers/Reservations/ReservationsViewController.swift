//
//  ViewController.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ReservationsViewController: UIViewController {
  
  // MARK: - IBOutlets & UI
  @IBOutlet private weak var tableView: UITableView!
  var addButton: UIBarButtonItem!
  
  // MARK: - Properties
  var viewModel: ReservationsViewModel!
  private let disposeBag = DisposeBag()

  // MARK: - Life cycles
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setup()
    binding()
  }

  // MARK: - Setup & Binding
  private func setup() {
    /// navigation
    title = "Reservations"
    addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
    navigationItem.rightBarButtonItem = addButton
    
    /// tableview
    tableView.register(ReservationCell.self)
    tableView.tableFooterView = UIView(frame: .zero)
    tableView.rx.setDelegate(self).disposed(by: disposeBag)
    
    tableView.refreshControl = UIRefreshControl()
    tableView.refreshControl?.tintColor = .red
  }
  
  private func binding() {
    /// input
    let input = inputViewModel()
    
    /// output
    let output = viewModel.transform(input: input)
    handleOutput(output: output)
  }
  
  private func inputViewModel() -> ReservationsViewModel.Input {
    /// Reload data when:
    /// - initial list Rooms
    /// - pull to refresh
    let pullToRefresh = tableView.refreshDriver()
    let refresh = Driver.merge(pullToRefresh, Driver.just(()))
    
    return ReservationsViewModel.Input(addReservation: addButton.rx.tap.asDriver(),
                                       deleteReservation: tableView.rx.itemDeleted.asDriver(),
                                       selectedReservation: tableView.rx.itemSelected.asDriver(),
                                       refresh: refresh)
  }
  
  private func handleOutput(output: ReservationsViewModel.Output) {
    output.reservations
      .do(onNext: { [weak self] _ in
        self?.endRefreshing()
      })
      .drive(tableView.rx.items(cellIdentifier: "ReservationCell", cellType: ReservationCell.self)){ _, reservation, cell in
        cell.data = reservation
      }.disposed(by: disposeBag)
    
    output.selectedReservation.drive(onNext: { [weak self] (reservation) in
      self?.toReservationDetails(data: reservation)
    }).disposed(by: disposeBag)
    
    output.error.drive(onNext: { [weak self] (message) in
      self?.endRefreshing()
      self?.showPopup(title: "Error", message: message)
    }).disposed(by: disposeBag)
  }
}

// MARK: - Private functions
extension ReservationsViewController {
  private func showPopup(title: String? = nil, message: String, titleButton: String = "OK", handler: ((UIAlertAction) -> Void)? = nil) {
    
    /// Dismissing the current alertviewcontroller if there is the one visible on the screen
    if let presenting = self.presentingViewController as? UIAlertController {
      presenting.dismiss(animated: false, completion: nil)
    }
    let alertViewController = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: titleButton, style: .default, handler: handler)
    alertViewController.addAction(okAction)
    
    self.present(alertViewController, animated: true)
  }
  
  private func endRefreshing() {
    tableView.refreshControl?.endRefreshing()
  }
}

// MARK: - Router
extension ReservationsViewController {
  private func toReservationDetails(data: Reservation) {
    let vc = ReservationDetailsViewController()
    vc.viewModel = ReservationDetailsViewModel(reservation: data)
    
    self.navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - Tableview Delegate
extension ReservationsViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
    return .delete
  }
}
