//
//  ReservationDetailsViewController.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ReservationDetailsViewController: UIViewController {
  
  // MARK: - IBoutlets
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var timeLabel: UILabel!
  @IBOutlet private weak var imageView: UIImageView!
  
  // MARK: - Properties
  var viewModel: ReservationDetailsViewModel!
  private let disposeBag = DisposeBag()
  
  // MARK: - Life cycles
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setup()
    binding()
  }
  
  // MARK: - Setup & Binding
  private func setup() {
    title = "Details"
  }
  
  private func binding() {
    /// input
    let input = ReservationDetailsViewModel.Input()
    
    /// output
    let output = viewModel.transform(input: input)
    output.reservation.drive(onNext: { [weak self] (reservation) in
      self?.renderData(data: reservation)
    }).disposed(by: disposeBag)
  }
  
  private func renderData(data: Reservation) {
    let date = data.startAt?.dateToString(DateFormat.dateExamination2.rawValue)
    let startTime = data.startAt?.dateToString(DateFormat.hourMinute.rawValue)
    let endTime = data.endAt?.dateToString(DateFormat.hourMinute.rawValue)
    timeLabel.text = date.unWrap + " " + startTime.unWrap + " - " + endTime.unWrap
    
    titleLabel.text = data.doctor?.name
    
    if let url = data.doctor?.avatarURL {
      imageView.setImageUrl(url)
    }
  }
}
