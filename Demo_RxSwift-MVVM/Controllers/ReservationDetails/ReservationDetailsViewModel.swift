//
//  ReservationDetailsViewModel.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class ReservationDetailsViewModel: ViewModelType {
  
  var reservation: Reservation
  
  init(reservation: Reservation) {
    self.reservation = reservation
  }
  
  func transform(input: ReservationDetailsViewModel.Input) -> ReservationDetailsViewModel.Output {
    return Output(reservation: Driver.just(reservation))
  }
  
}

extension ReservationDetailsViewModel {
  struct Input {
    
  }
  
  struct Output {
    let reservation: Driver<Reservation>
  }
}
