//
//  AppDelegate.swift
//  Demo_RxSwift-MVVM
//
//  Copyright © 2019 Phan Vu Huy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    let window = UIWindow(frame: UIScreen.main.bounds)
    
    let vc = ReservationsViewController()
    vc.viewModel = ReservationsViewModel()
    window.rootViewController = UINavigationController(rootViewController: vc)
    
    self.window = window
    self.window?.makeKeyAndVisible()
    
    return true
  }
}

